# I Want Compute !

**(!)** - Security & Access Control are not part of this solution description.

## `h2o` workload

The following guide is using the [h2o](https://www.h2o.ai/download/#h2o) Open-Source ML Framework as the example workload which needs to be executed on the IBM Cloud.

- [Productionizing](https://h2o-release.s3.amazonaws.com/h2o/rel-turing/1/docs-website/h2o-docs/productionizing.html#) `h2o` workload is generally by using one of the interface language like `Python` or `R`.
- Several `h2o` instances can form a [cluster](https://h2o-release.s3.amazonaws.com/h2o/rel-turing/1/docs-website/h2o-docs/starting-h2o.html#from-the-command-line)

Though we use `h2o` as an example, the guide can be applied to any workload.

## 1 - The New Kid on the Block, K8s with CodeEngine

**(!) Code-Engine is currently Beta - and prone to change**

> This solution is based on `docker` images, and expected to have a docker image available, or `Dockerfile` where the necesarry application is run ready.

With the same [image repository](https://gitlab.com/whendrik/cloudfoundry-custom-image) as for the Cloud Foundry app, computer can be provisioned with the [IBM Code Engine](https://cloud.ibm.com/docs/codeengine?topic=codeengine-getting-started)

The docker images can also be [build](https://cloud.ibm.com/docs/codeengine?topic=codeengine-plan-build) from `git` (source) or a `Dockerfile` directly.

With a free-account, container runtime limits are 8 vCPU with 32 GB ram.

![codeengine](images/codeengine.gif)

With a basic [h2o](https://gitlab.com/whendrik/ubuntu-based-h20) dockerfile, the following command will create a `h2o` app

```
ibmcloud ce app create --name h2o_instance --image us.icr.io/willemh/h2o:2 --cpu 2 -m 2Gi -p 54321 --rs willemh
```

Make sure that the command to start specifies the maximum memory, as automatic memory allocation might fail in k8s environments, e.g;

```
java -Xmx1g -jar h2o.jar
```

Open Questions:

- Can `h2o` run in cluster mode, with multiple applications forming a cluster?

## 2 - The Agile Compute, Next Gen VPC (Generation 2 VPC Cloud Infratructure)

**Proven Solution - Needed infrastructure provisioning can be automated with API and/or Terraform.**

To automate the provision service, use [TerraForm](https://cloud.ibm.com/docs/terraform?topic=terraform-getting-started) or the [IBM Cloud CLI](https://cloud.ibm.com/docs/cli?topic=cli-getting-started).

See a [minimal example](https://gitlab.com/whendrik/ibm-cloud-terraform-example) to provision a basic Ubuntu server, connected to the internet and `ssh`-able.

![virtual_prices](images/vpc.gif)

* Installing `openjdk-11-jre-headless` takes between 10 and 20 seconds. This is possibly fast enough to install-on-demand. Alternatively a images can be created with the necessary prerequisites installed.
* The .gif above is not showing binding a [Floating IP](https://cloud.ibm.com/docs/vpc?topic=vpc-vsi_is_connecting_linux) which is needed to connect. 
* [h2o](http://docs.h2o.ai/h2o/latest-stable/h2o-docs/starting-h2o.html#from-the-command-line) support command line option, `name [clustername]` which will form a cluster. *"Nodes with the same cluster name will form an H2O cluster (also known as an H2O cloud)."*. This is tested and works perfect, with use of the `-flatfile` refering to a document with the `internal IP adresses`.

---

Alternatively, [IBM Cloud Classic](https://cloud.ibm.com/docs/cloud-infrastructure?topic=cloud-infrastructure-compare-infrastructure) can be used, which is conceptully the same, or VPC GEN 1.

## 3 - The Millennial, Cloud (native) Applications with Cloud-Foundry.

(!) Not recommended - not tested, though technical possible

> This solution is based on `docker` images, and expected to have a docker image available, or `Dockerfile` where the necesarry application is run ready.

The Cloud Foundry framework can be used to create Web Applications.

I have made a [guide](https://gitlab.com/whendrik/cloudfoundry-custom-image) which outlines the steps to create a custom image.

Cloud Foundry applications are expected to scale horizontally, therefore instances have limited memory and no setting for # vCPU.

The use case for a spiky h2o load doesn't fit the cloudfoundry framework well, as it is commonly used for smaller web-applications that scale horizontally.

